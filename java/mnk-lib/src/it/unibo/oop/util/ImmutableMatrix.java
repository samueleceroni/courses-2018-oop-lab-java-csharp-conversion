package it.unibo.oop.util;

public interface ImmutableMatrix<E> extends BaseMatrix<E> {

    static <X> ImmutableMatrix<X> wrap(BaseMatrix<X> matrix) {
        return new ImmutableMatrixDecorator<>(matrix);
    }

}
